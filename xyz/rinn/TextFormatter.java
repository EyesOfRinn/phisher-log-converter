package xyz.rinn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class TextFormatter {
	protected ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	protected URL resource = classLoader.getResource("Formatter.jar");
	protected File location = new File(resource.getPath());
	
	protected File inputText = new File(location.toString().replace("Formatter.jar", "input.txt").replace("%20", " "));
	protected File outputText = new File(location.toString().replace("Formatter.jar", "output.txt").replace("%20", " "));
	
	protected ArrayList<String> outputLines = new ArrayList<String>();
	
	public static void main(String[] args) throws IOException {
		TextFormatter formatter = new TextFormatter();
		
		BufferedReader br = new BufferedReader(new FileReader(formatter.inputText));
		StringBuilder inputText = new StringBuilder();
		
		char[] buffer = new char[1024];
		
		for (int c = br.read(buffer); c > 0; c = br.read(buffer)) {
			for (int i = 0; i < c; i++) {
				inputText.append(buffer[i]);
			}
		}
		String[] lines = inputText.toString().replace("\r", "").split("\n");
		
		String text = "";
		
		for (String line : lines)
		{
			if (line.startsWith("IP") || line.startsWith("remember") || line.equalsIgnoreCase("")) {
				continue;
			}
			
			if (line.startsWith("email") || line.startsWith("username")) {
				text = text + line + ":";
			} else {
				text = text + line + '\n';
			}
		}
		text = replaceMatching(text, ":username=", ":password=");
		text = text.replace("email= ", "");
		text = text.replace("email=", "");
		text = text.replace("username= ", "");
		text = text.replace("username=", "");
		text = text.replace("password=", "");
		text = text.replace("::", ":");
		
		BufferedWriter out = null;
		FileWriter fstream = new FileWriter(formatter.outputText, true);
		out = new BufferedWriter(fstream);
		out.write(text);
		out.flush();
		out.close();
	}
	
	public static String replaceMatching(String input, String lowerBound, String upperBound){
	      String result = input.replaceAll("(.*?" + lowerBound + ")" + "(.*?)" + "(" + upperBound + ".*)", "$1$3");
	      return result;
	 }
}
